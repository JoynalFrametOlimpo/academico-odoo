# -*- coding: utf-8 -*-

from odoo import models, fields, api

class academico(models.Model):
    _name = 'academico.alumno'
    _description = 'Academico Alumno'

    name = fields.Char( string= "Nombre", required =True, help= "Se escribe el nombre del alumno" )
    edad = fields.Integer(string="Edad", required=False,help="Edad en números" )
    estado_civil = fields.Selection(string="Estado Civil", selection=[('Casado', 'casado'), ('Soltero', 'soltero'), ('Viudo', 'viudo'), ], required=False, help="Estado civil de la persona" )
    es_profesor = fields.Boolean(string="Profesor" )
    fecha_nacimiento = fields.Date(string="Fecha de Nacimiento", required=False, help="Fecha de Nacimiento")
    observacion = fields.Text(string="Observación", required=False, help="Realiza alguna nota")
    prueba = fields.Char(string="Prueba", required=False, )
    prueba2 = fields.Char(string="Prueba2")
    prueba3 = fields.Char(string="Prueba3", required=False, )
    prueba4 = fields.Char(string="Prueba4", required=False, )
    prueba5 = fields.Char(string="Prueba5", required=False, )

    curso_id = fields.Many2one(comodel_name="academico.curso", string="Curso", required=False, index=True)

class academico_curso(models.Model):
    _name = 'academico.curso'

    name = fields.Char(string="Nombre")
    numero_horas = fields.Integer(string="Numero de Horas")






